module.exports = function(grunt) {
	
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Configurable paths
  var config = {
    app: {
      styles: 'web/app/styles',
      scripts: 'web/app/scripts'
    },
    dist: {
      styles: 'web/dist/styles',
      scripts: 'web/dist/scripts'
    },
    tmp: {
      styles: 'web/dist/styles',
      scripts: 'web/dist/scripts'
    }  
  };
  // Project configuration.
  grunt.initConfig({
    config: config,
    jshint: {
      files: ['Gruntfile.js', '<%= config.app.scripts %>/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    sass: {
      options: {
        loadPath: [
          'lib/vendor-bower/bootstrap-sass-official/assets/stylesheets'
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app.styles %>',
          src: ['*.sass'],
          dest: '<%= config.dist.styles %>',
          ext: '.css'
        }]
      }
    },
    watch: {
      sass: {
        files: ['<%= config.app.styles %>{,*/}*.{scss,sass}'],
        tasks: ['sass:dist']
    },
    js: {
      files: ['<%= config.app.scripts %>{,*/}*.{js,json}'],
      tasks: ['jshint']
  }
}
});

  // Build task(s)
  grunt.registerTask('build', ['jshint']);

  // Watch task(s)
  grunt.registerTask('serve', ['watch']);
  
  // Default task(s).
  grunt.registerTask('default', ['jshint','sass']);

};